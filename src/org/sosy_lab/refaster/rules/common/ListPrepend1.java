// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.common;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.UseImportPolicy;
import java.util.Collection;
import org.sosy_lab.common.collect.Collections3;

class ListPrepend1<E> {

  @BeforeTemplate
  ImmutableList<E> builder(E elem, Collection<E> list) {
    return ImmutableList.<E>builder().add(elem).addAll(list).build();
  }

  @BeforeTemplate
  ImmutableList<E> builderWithExpectedSize(E elem, Collection<E> list, int size) {
    return ImmutableList.<E>builderWithExpectedSize(size).add(elem).addAll(list).build();
  }

  @SuppressWarnings("unchecked")
  @BeforeTemplate
  ImmutableList<E> fluentIterable(E elem, Collection<E> list) {
    return FluentIterable.of(elem).append(list).toList();
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  ImmutableList<? super E> asList(E elem, Collection<E> list) {
    return Collections3.elementAndList(elem, list);
  }
}
