// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

/**
 * This package contains refaster templates that make use of our Java library SoSy-Lab Common
 * (https://github.com/sosy-lab/java-common-lib/).
 */
package org.sosy_lab.refaster.rules.common;
