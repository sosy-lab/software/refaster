// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.common;

import com.google.common.collect.Collections2;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collection;
import org.sosy_lab.common.collect.Collections3;

class UnnecessaryTransformedListCopy<T, U> {

  @BeforeTemplate
  void transformedImmutableListCopy(
      Collection<T> source, com.google.common.base.Function<T, U> func, Collection<U> target) {
    target.addAll(Collections3.transformedImmutableListCopy(source, func));
  }

  @AfterTemplate
  void transform(
      Collection<T> source, com.google.common.base.Function<T, U> func, Collection<U> target) {
    target.addAll(Collections2.transform(source, func));
  }
}
