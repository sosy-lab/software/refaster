// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.common;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.UseImportPolicy;
import java.util.Collection;
import org.sosy_lab.common.collect.Collections3;

abstract class TransformedImmutableList<T, U> {

  @BeforeTemplate
  ImmutableList<U> fluentIterable(Collection<T> c, com.google.common.base.Function<T, U> func) {
    // FluentIterable.transform is Iterator-based and looses size
    return FluentIterable.from(c).transform(func).toList();
  }

  @BeforeTemplate
  ImmutableList<U> iterablesTransform(Collection<T> c, com.google.common.base.Function<T, U> func) {
    // Iterables.transform is Iterator-based and looses size
    return ImmutableList.copyOf(Iterables.transform(c, func));
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  ImmutableList<U> transformedImmutableListCopy(
      Collection<T> c, com.google.common.base.Function<T, U> func) {
    return Collections3.transformedImmutableListCopy(c, func);
  }
}
