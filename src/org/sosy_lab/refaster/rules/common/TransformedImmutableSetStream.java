// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.common;

import com.google.common.collect.ImmutableSet;
import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.MayOptionallyUse;
import com.google.errorprone.refaster.annotation.Placeholder;
import com.google.errorprone.refaster.annotation.UseImportPolicy;
import java.util.Collection;
import org.sosy_lab.common.collect.Collections3;

abstract class TransformedImmutableSetStream<T, U> {

  @Placeholder
  abstract U func(@MayOptionallyUse T t);

  @BeforeTemplate
  ImmutableSet<U> streamLambda(Collection<T> c) {
    return c.stream().map(x -> func(x)).collect(ImmutableSet.toImmutableSet());
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  ImmutableSet<U> transformedImmutableSetCopy(Collection<T> c) {
    return Collections3.transformedImmutableSetCopy(c, x -> func(x));
  }
}
