// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.common;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.UseImportPolicy;
import java.util.Collection;
import org.sosy_lab.common.collect.Collections3;

class ListAppend1<E> {

  @BeforeTemplate
  ImmutableList<E> builder(Collection<E> list, E elem) {
    return ImmutableList.<E>builder().addAll(list).add(elem).build();
  }

  @BeforeTemplate
  ImmutableList<E> builderWithExpectedSize(Collection<E> list, E elem, int size) {
    return ImmutableList.<E>builderWithExpectedSize(size).addAll(list).add(elem).build();
  }

  @SuppressWarnings("unchecked")
  @BeforeTemplate
  ImmutableList<E> fluentIterable(Collection<E> list, E elem) {
    return FluentIterable.from(list).append(elem).toList();
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  ImmutableList<? super E> asList(Collection<E> list, E elem) {
    return Collections3.listAndElement(list, elem);
  }
}
