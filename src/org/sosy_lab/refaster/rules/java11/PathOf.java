// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java11;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.Repeated;
import java.nio.file.Path;
import java.nio.file.Paths;

class PathOf {

  @BeforeTemplate
  Path get(String first, @Repeated String more) {
    return Paths.get(first, more);
  }

  @AfterTemplate
  Path of(String first, @Repeated String more) {
    return Path.of(first, more);
  }
}
