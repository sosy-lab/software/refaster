// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java11;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

class PathOfURI {

  @BeforeTemplate
  Path get(URI u) {
    return Paths.get(u);
  }

  @AfterTemplate
  Path of(URI u) {
    return Path.of(u);
  }
}
