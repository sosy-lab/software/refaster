// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.ImmutableMultiset;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class StreamToImmutableMultiset<T> {

  @BeforeTemplate
  ImmutableMultiset<T> streamToList(Stream<T> s) {
    return ImmutableMultiset.copyOf(s.collect(Collectors.toList()));
  }

  @AfterTemplate
  ImmutableMultiset<T> toImmutableSet(Stream<T> s) {
    return s.collect(ImmutableMultiset.toImmutableMultiset());
  }
}
