// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.FluentIterable;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.AlsoNegation;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class FluentIterableIsEmpty<T> {

  @BeforeTemplate
  boolean sizeEqualsZero(FluentIterable<T> it) {
    return it.size() == 0;
  }

  @BeforeTemplate
  boolean sizeLessOrEqualZero(FluentIterable<T> it) {
    // relevant as negated case "it.size() > 0"
    return it.size() <= 0;
  }

  @AfterTemplate
  @AlsoNegation
  boolean isEmpty(FluentIterable<T> it) {
    return it.isEmpty();
  }
}
