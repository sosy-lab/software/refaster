// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class IterableToListCopyForSize<T> {

  @BeforeTemplate
  int newArrayList(Iterable<T> it) {
    return Lists.newArrayList(it).size();
  }

  @BeforeTemplate
  int immutableList(Iterable<T> it) {
    return ImmutableList.copyOf(it).size();
  }

  @AfterTemplate
  int iterableSize(Iterable<T> it) {
    return Iterables.size(it);
  }
}
