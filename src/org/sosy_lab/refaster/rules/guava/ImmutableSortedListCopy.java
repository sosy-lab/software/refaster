// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class ImmutableSortedListCopy<T extends Comparable<T>> {

  @BeforeTemplate
  ImmutableList<T> orderingImmutableSortedCopyOf(Iterable<T> source) {
    return Ordering.natural().immutableSortedCopy(source);
  }

  @AfterTemplate
  ImmutableList<T> immutableListSortedCopyOf(Iterable<T> source) {
    return ImmutableList.sortedCopyOf(source);
  }
}
