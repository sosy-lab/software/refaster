// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2024 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.Repeated;
import java.util.List;

@SuppressWarnings("unchecked")
class FluentIterableOf<T> {

  @BeforeTemplate
  FluentIterable<T> fromNewArrayList(@Repeated T elem) {
    return FluentIterable.from(Lists.newArrayList(elem));
  }

  @BeforeTemplate
  FluentIterable<T> fromImmutableList(@Repeated T elem) {
    return FluentIterable.from(ImmutableList.of(elem));
  }

  @BeforeTemplate
  FluentIterable<T> fromList(@Repeated T elem) {
    return FluentIterable.from(List.of(elem));
  }

  @AfterTemplate
  FluentIterable<T> of(@Repeated T elem) {
    return FluentIterable.of(elem);
  }
}
