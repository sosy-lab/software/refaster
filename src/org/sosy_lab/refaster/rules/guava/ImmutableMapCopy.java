// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.HashMap;
import java.util.Map;

class ImmutableMapCopy<K, V> {

  @BeforeTemplate
  ImmutableMap<K, V> viaHashMap(Map<K, V> m) {
    return ImmutableMap.copyOf(new HashMap<>(m));
  }

  @BeforeTemplate
  ImmutableMap<K, V> viaHashMap2(Map<K, V> m) {
    return ImmutableMap.copyOf(Maps.newHashMap(m));
  }

  @BeforeTemplate
  ImmutableMap<K, V> viaMap(Map<K, V> m) {
    return ImmutableMap.copyOf(Map.copyOf(m));
  }

  @AfterTemplate
  ImmutableMap<K, V> direct(Map<K, V> m) {
    return ImmutableMap.copyOf(m);
  }
}
