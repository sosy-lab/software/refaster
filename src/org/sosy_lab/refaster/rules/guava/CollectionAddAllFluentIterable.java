// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// The original source of this file is https://errorprone.info/docs/refaster
//
// SPDX-FileCopyrightText: The Error Prone Authors
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.FluentIterable;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collection;

class CollectionAddAllFluentIterable<E> {

  @BeforeTemplate
  void addImmutableListCopy(FluentIterable<E> source, Collection<E> target) {
    target.addAll(source.toList());
  }

  @BeforeTemplate
  void addInLoop(FluentIterable<E> source, Collection<E> target) {
    for (E e : source) {
      target.add(e);
    }
  }

  @AfterTemplate
  void addAll(FluentIterable<E> source, Collection<E> target) {
    source.copyInto(target);
  }
}
