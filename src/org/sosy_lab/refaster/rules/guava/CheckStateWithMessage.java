// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.base.Preconditions;
import com.google.errorprone.matchers.CompileTimeConstantExpressionMatcher;
import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.Matches;
import com.google.errorprone.refaster.annotation.UseImportPolicy;

class CheckStateWithMessage {

  @BeforeTemplate
  void ifThrow(boolean b, @Matches(CompileTimeConstantExpressionMatcher.class) String msg) {
    // Require constant message because with checkArgument message creation is not lazy
    if (b) {
      throw new IllegalStateException(msg);
    }
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  void checkState(boolean b, String msg) {
    Preconditions.checkState(!b, msg);
  }
}
