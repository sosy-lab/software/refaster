// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class ImmutableSetCopy<T> {

  @BeforeTemplate
  ImmutableSet<T> viaStream(Collection<T> c) {
    return c.stream().collect(ImmutableSet.toImmutableSet());
  }

  @BeforeTemplate
  ImmutableSet<T> viaFluentIterable(Iterable<T> c) {
    return FluentIterable.from(c).toSet();
  }

  @BeforeTemplate
  ImmutableSet<T> viaHashSet(Collection<T> c) {
    return ImmutableSet.copyOf(new HashSet<>(c));
  }

  @BeforeTemplate
  ImmutableSet<T> viaHashSet2(Iterable<T> c) {
    return ImmutableSet.copyOf(Sets.newHashSet(c));
  }

  @BeforeTemplate
  ImmutableSet<T> viaSet(Collection<T> c) {
    return ImmutableSet.copyOf(Set.copyOf(c));
  }

  @BeforeTemplate
  ImmutableSet<T> viaArrayList(Collection<T> c) {
    return ImmutableSet.copyOf(new ArrayList<>(c));
  }

  @BeforeTemplate
  ImmutableSet<T> viaArrayList2(Iterable<T> c) {
    return ImmutableSet.copyOf(Lists.newArrayList(c));
  }

  @BeforeTemplate
  ImmutableSet<T> viaList(Collection<T> c) {
    return ImmutableSet.copyOf(List.copyOf(c));
  }

  @AfterTemplate
  ImmutableSet<T> direct(Iterable<T> c) {
    return ImmutableSet.copyOf(c);
  }
}
