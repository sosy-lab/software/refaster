// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.Streams;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

class IterableStream<T> {

  @BeforeTemplate
  Stream<T> spliterator(Iterable<T> it) {
    return StreamSupport.stream(it.spliterator(), false);
  }

  @AfterTemplate
  Stream<T> streams(Iterable<T> it) {
    return Streams.stream(it);
  }
}
