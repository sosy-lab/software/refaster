// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.ImmutableSortedSet;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Comparator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class StreamToImmutableSortedSet<T> {

  @BeforeTemplate
  ImmutableSortedSet<T> streamToList(Stream<T> s, Comparator<T> comp) {
    return ImmutableSortedSet.copyOf(comp, s.collect(Collectors.toList()));
  }

  @BeforeTemplate
  ImmutableSortedSet<T> streamToSet(Stream<T> s, Comparator<T> comp) {
    return ImmutableSortedSet.copyOf(comp, s.collect(Collectors.toSet()));
  }

  @BeforeTemplate
  ImmutableSortedSet<T> streamToSortedList(Stream<T> s, Comparator<T> comp) {
    return ImmutableSortedSet.copyOf(comp, s.sorted(comp).collect(Collectors.toList()));
  }

  @BeforeTemplate
  ImmutableSortedSet<T> streamToSortedSet(Stream<T> s, Comparator<T> comp) {
    return ImmutableSortedSet.copyOf(comp, s.sorted(comp).collect(Collectors.toSet()));
  }

  @AfterTemplate
  ImmutableSortedSet<T> toImmutableSortedSet(Stream<T> s, Comparator<T> comp) {
    return s.collect(ImmutableSortedSet.toImmutableSortedSet(comp));
  }
}
