// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.base.Preconditions;
import com.google.errorprone.matchers.CompileTimeConstantExpressionMatcher;
import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.Matches;
import com.google.errorprone.refaster.annotation.UseImportPolicy;

class CheckArgumentWithLazyMessage1 {

  @BeforeTemplate
  void eager(
      boolean b, @Matches(CompileTimeConstantExpressionMatcher.class) String msg1, Object o1) {
    Preconditions.checkArgument(b, msg1 + o1);
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  void lazy(boolean b, String msg1, Object o1) {
    Preconditions.checkArgument(b, msg1 + "%s", o1);
  }
}
