// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class ImmutableListCopy<T> {

  @BeforeTemplate
  ImmutableList<T> viaStream(Collection<T> c) {
    return c.stream().collect(ImmutableList.toImmutableList());
  }

  @BeforeTemplate
  ImmutableList<T> viaFluentIterable(Iterable<T> c) {
    return FluentIterable.from(c).toList();
  }

  @BeforeTemplate
  ImmutableList<T> viaArrayList(Collection<T> c) {
    return ImmutableList.copyOf(new ArrayList<>(c));
  }

  @BeforeTemplate
  ImmutableList<T> viaArrayList2(Iterable<T> c) {
    return ImmutableList.copyOf(Lists.newArrayList(c));
  }

  @BeforeTemplate
  ImmutableList<T> viaList(Collection<T> c) {
    return ImmutableList.copyOf(List.copyOf(c));
  }

  @AfterTemplate
  ImmutableList<T> direct(Iterable<T> c) {
    return ImmutableList.copyOf(c);
  }
}
