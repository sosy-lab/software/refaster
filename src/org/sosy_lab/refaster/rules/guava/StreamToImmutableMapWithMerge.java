// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.ImmutableMap;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class StreamToImmutableMapWithMerge<T, K, V> {

  @BeforeTemplate
  ImmutableMap<K, V> streamToList(
      Stream<T> s,
      Function<? super T, ? extends K> keyFunction,
      Function<? super T, ? extends V> valueFunction,
      BinaryOperator<V> mergeFunction) {
    return ImmutableMap.copyOf(
        s.collect(Collectors.toMap(keyFunction, valueFunction, mergeFunction)));
  }

  @AfterTemplate
  ImmutableMap<K, V> toImmutableList(
      Stream<T> s,
      Function<? super T, ? extends K> keyFunction,
      Function<? super T, ? extends V> valueFunction,
      BinaryOperator<V> mergeFunction) {
    return s.collect(ImmutableMap.toImmutableMap(keyFunction, valueFunction, mergeFunction));
  }
}
