// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// The original source of this file is https://errorprone.info/docs/refaster
//
// SPDX-FileCopyrightText: The Error Prone Authors
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collection;

class CollectionAddAllIterable<E> {

  @BeforeTemplate
  void addInLoop(Iterable<E> source, Collection<E> target) {
    for (E e : source) {
      target.add(e);
    }
  }

  @BeforeTemplate
  void addImmutableListCopy(Iterable<E> source, Collection<E> target) {
    target.addAll(ImmutableList.copyOf(source));
  }

  @BeforeTemplate
  void addArrayListCopy(Iterable<E> source, Collection<E> target) {
    target.addAll(Lists.newArrayList(source));
  }

  @BeforeTemplate
  void fluentIterableCopyInto(Iterable<E> source, Collection<E> target) {
    FluentIterable.from(source).copyInto(target);
  }

  @AfterTemplate
  void addAll(Iterable<E> source, Collection<E> target) {
    Iterables.addAll(target, source);
  }
}
