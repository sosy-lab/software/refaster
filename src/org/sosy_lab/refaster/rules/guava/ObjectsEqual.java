// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Objects;

class ObjectsEqual {

  @BeforeTemplate
  boolean guavaEquals(Object a, Object b) {
    return com.google.common.base.Objects.equal(a, b);
  }

  @AfterTemplate
  boolean javaEquals(Object a, Object b) {
    return Objects.equals(a, b);
  }
}
