// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.FluentIterable;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class FluentIterableSize<T> {

  @BeforeTemplate
  int toList(FluentIterable<T> it) {
    return it.toList().size();
  }

  @AfterTemplate
  int size(FluentIterable<T> it) {
    return it.size();
  }
}
