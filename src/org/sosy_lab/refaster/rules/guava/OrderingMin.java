// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.Ordering;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

class OrderingMin<T> {

  @BeforeTemplate
  T orderingMin(Collection<T> coll, Comparator<T> comp) {
    return Ordering.from(comp).min(coll);
  }

  @AfterTemplate
  T collectionsMin(Collection<T> coll, Comparator<T> comp) {
    return Collections.min(coll, comp);
  }
}
