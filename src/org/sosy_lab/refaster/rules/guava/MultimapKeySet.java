// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava;

import com.google.common.collect.Multimap;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Set;

class MultimapKeySet<K> {

  @BeforeTemplate
  Set<K> asMapKeySet(Multimap<K, ?> m) {
    return m.asMap().keySet();
  }

  @AfterTemplate
  Set<K> keySet(Multimap<K, ?> m) {
    return m.keySet();
  }
}
