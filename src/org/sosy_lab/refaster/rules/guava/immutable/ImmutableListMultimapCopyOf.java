// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class ImmutableListMultimapCopyOf<K, V> {

  @BeforeTemplate
  ImmutableMultimap<K, V> immutableMultimapCopyOf(Multimap<K, V> m) {
    return ImmutableMultimap.copyOf(m);
  }

  @AfterTemplate
  ImmutableListMultimap<K, V> immutableListMultimapCopyOf(Multimap<K, V> m) {
    return ImmutableListMultimap.copyOf(m);
  }
}
