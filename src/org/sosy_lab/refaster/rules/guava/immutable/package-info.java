// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

/**
 * This package contains refaster templates for using Guava's immutable data structures instead of
 * immutable data structures supplied by Java itself. Guava's data structures have the advantage
 * that they provide a visible type that guarantees immutability. If we use that type consistently,
 * this increases clarity. Furthermore, defensive copies of immutable data structures are only
 * avoided if we consistently use the same set of types.
 */
package org.sosy_lab.refaster.rules.guava.immutable;
