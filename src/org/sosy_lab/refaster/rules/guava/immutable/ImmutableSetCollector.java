// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableSet;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

class ImmutableSetCollector<T> {

  @BeforeTemplate
  Collector<T, ?, Set<T>> toUnmodifiableSet() {
    return Collectors.toUnmodifiableSet();
  }

  @AfterTemplate
  Collector<T, ?, ImmutableSet<T>> toImmutableSet() {
    return ImmutableSet.toImmutableSet();
  }
}
