// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableList;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collection;
import java.util.List;

class ImmutableListCopyOf<T> {

  @BeforeTemplate
  List<T> usingBuilder(Iterable<T> c) {
    ImmutableList.Builder<T> builder = ImmutableList.builder();
    builder.addAll(c);
    return builder.build();
  }

  @BeforeTemplate
  List<T> listCopyOf(Collection<T> c) {
    return List.copyOf(c);
  }

  @AfterTemplate
  ImmutableList<T> immutableListCopyOf(Iterable<T> c) {
    return ImmutableList.copyOf(c);
  }
}
