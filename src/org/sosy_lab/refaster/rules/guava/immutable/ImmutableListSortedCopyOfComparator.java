// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

class ImmutableListSortedCopyOfComparator<T> {

  @BeforeTemplate
  List<T> usingStreamFromCollection(Comparator<? super T> comparator, Collection<T> it) {
    return it.stream().sorted(comparator).collect(ImmutableList.toImmutableList());
  }

  @BeforeTemplate
  List<T> usingStreamFromFluentIterable(Comparator<? super T> comparator, FluentIterable<T> it) {
    return it.stream().sorted(comparator).collect(ImmutableList.toImmutableList());
  }

  @AfterTemplate
  ImmutableList<T> immutableListCopyOf(Comparator<? super T> comparator, Iterable<T> it) {
    return ImmutableList.sortedCopyOf(comparator, it);
  }
}
