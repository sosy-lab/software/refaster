// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// The original source of this file is https://errorprone.info/docs/refaster
//
// SPDX-FileCopyrightText: The Error Prone Authors
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableMap;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Map;

class ImmutableMapBuilderAddAllIterable<K, V> {

  @BeforeTemplate
  void addInLoop(Iterable<Map.Entry<K, V>> source, ImmutableMap.Builder<K, V> target) {
    for (Map.Entry<K, V> entry : source) {
      target.put(entry.getKey(), entry.getValue());
    }
  }

  @AfterTemplate
  void addAll(Iterable<Map.Entry<K, V>> source, ImmutableMap.Builder<K, V> target) {
    target.putAll(source);
  }
}
