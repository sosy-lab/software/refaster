// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableSet;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collection;
import java.util.Set;

class ImmutableSetCopyOf<T> {

  @BeforeTemplate
  Set<T> usingBuilder(Iterable<T> c) {
    ImmutableSet.Builder<T> builder = ImmutableSet.builder();
    builder.addAll(c);
    return builder.build();
  }

  @BeforeTemplate
  Set<T> setCopyOf(Collection<T> c) {
    return Set.copyOf(c);
  }

  @AfterTemplate
  ImmutableSet<T> immutableSetCopyOf(Iterable<T> c) {
    return ImmutableSet.copyOf(c);
  }
}
