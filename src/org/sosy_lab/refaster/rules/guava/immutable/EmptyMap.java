// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableMap;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collections;
import java.util.Map;

class EmptyMap<K, V> {

  @BeforeTemplate
  Map<K, V> emptyMap() {
    return Collections.emptyMap();
  }

  @SuppressWarnings("unchecked")
  @BeforeTemplate
  Map<K, V> emptyMapField() {
    return Collections.EMPTY_MAP;
  }

  @BeforeTemplate
  Map<K, V> mapOf() {
    return Map.of();
  }

  @AfterTemplate
  Map<K, V> immutableMap() {
    return ImmutableMap.of();
  }
}
