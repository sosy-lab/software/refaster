// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// The original source of this file is https://errorprone.info/docs/refaster
//
// SPDX-FileCopyrightText: The Error Prone Authors
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableCollection;
import com.google.errorprone.matchers.CompileTimeConstantExpressionMatcher;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.Matches;

class ImmutableCollectionBuilderAddAll<E> {

  @BeforeTemplate
  void addInLoop(
      Iterable<E> source,
      // TODO: CompileTimeConstantExpressionMatcher is overly strict and will never match such
      // expressions, but we would need a matcher that does currently not exist. Cf.
      // https://gitlab.com/sosy-lab/software/cpachecker/-/commit/e1d219f168a43b5a40af8ea34019e30a895e659a#note_390493416
      // https://github.com/google/error-prone/issues/1497
      @Matches(CompileTimeConstantExpressionMatcher.class) ImmutableCollection.Builder<E> target) {
    for (E e : source) {
      target.add(e);
    }
  }

  @AfterTemplate
  void addAll(Iterable<E> source, ImmutableCollection.Builder<E> target) {
    target.addAll(source);
  }
}
