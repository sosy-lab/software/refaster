// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.errorprone.refaster.Refaster;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collections;
import java.util.List;
import java.util.Set;

class SingletonList<T> {

  @BeforeTemplate
  List<T> listOf(T element) {
    return List.of(element);
  }

  /* TODO enable after https://github.com/google/error-prone/issues/1320 is fixed
  @BeforeTemplate
  List<T> singletonList(@Matches(IsNonNullMatcher.class) T element) {
    return Collections.singletonList(element);
  }
  */

  @SuppressWarnings("unchecked")
  @BeforeTemplate
  List<T> copyOfSingletonList(T element) {
    return ImmutableList.copyOf(
        Refaster.anyOf(
            Collections.singletonList(element),
            Collections.singleton(element),
            Lists.newArrayList(element),
            Sets.newHashSet(element),
            List.of(element),
            Set.of(element)));
  }

  @AfterTemplate
  ImmutableList<T> immutableListOf(T element) {
    return ImmutableList.of(element);
  }
}
