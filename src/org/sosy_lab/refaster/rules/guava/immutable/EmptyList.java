// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableList;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collections;
import java.util.List;

class EmptyList<T> {

  @BeforeTemplate
  List<T> emptyList() {
    return Collections.emptyList();
  }

  @SuppressWarnings("unchecked")
  @BeforeTemplate
  List<T> emptyListField() {
    return Collections.EMPTY_LIST;
  }

  @BeforeTemplate
  List<T> mapOf() {
    return List.of();
  }

  @AfterTemplate
  List<T> immutableList() {
    return ImmutableList.of();
  }
}
