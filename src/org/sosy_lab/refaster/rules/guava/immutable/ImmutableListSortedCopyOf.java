// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collection;
import java.util.List;

class ImmutableListSortedCopyOf<T extends Comparable<T>> {

  @BeforeTemplate
  List<T> usingStreamFromCollection(Collection<T> it) {
    return it.stream().sorted().collect(ImmutableList.toImmutableList());
  }

  @BeforeTemplate
  List<T> usingStreamFromFluentIterable(FluentIterable<T> it) {
    return it.stream().sorted().collect(ImmutableList.toImmutableList());
  }

  @AfterTemplate
  ImmutableList<T> immutableListCopyOf(Iterable<T> it) {
    return ImmutableList.sortedCopyOf(it);
  }
}
