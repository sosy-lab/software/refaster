// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableCollection;
import com.google.errorprone.refaster.Refaster;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.stream.Stream;

class StreamAfterBuild<E> {

  @BeforeTemplate
  Stream<E> streamAfterBuild(ImmutableCollection.Builder<E> builder) {
    return builder.build().stream();
  }

  @AfterTemplate
  Stream<E> streamAfterBuildWithComment(ImmutableCollection.Builder<E> builder) {
    return Refaster.emitCommentBefore(
        "TODO refactor: copying into list is likely unnecessary before using stream()",
        builder.build().stream());
  }
}
