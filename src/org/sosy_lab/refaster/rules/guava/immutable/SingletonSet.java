// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.errorprone.refaster.Refaster;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collections;
import java.util.List;
import java.util.Set;

class SingletonSet<T> {

  @BeforeTemplate
  Set<T> setOf(T element) {
    return Set.of(element);
  }

  /* TODO enable after https://github.com/google/error-prone/issues/1320 is fixed
  @BeforeTemplate
  Set<T> singletonSet(@Matches(IsNonNullMatcher.class) T element) {
    return Collections.singleton(element);
  }
  */

  @SuppressWarnings("unchecked")
  @BeforeTemplate
  Set<T> copyOfSingletonSet(T element) {
    return ImmutableSet.copyOf(
        Refaster.anyOf(
            Collections.singletonList(element),
            Collections.singleton(element),
            Lists.newArrayList(element),
            Sets.newHashSet(element),
            List.of(element),
            Set.of(element)));
  }

  @AfterTemplate
  ImmutableSet<T> immutableSetOf(T element) {
    return ImmutableSet.of(element);
  }
}
