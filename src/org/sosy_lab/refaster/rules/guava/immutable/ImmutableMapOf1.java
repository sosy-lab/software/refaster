// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableMap;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class ImmutableMapOf1<K, V> {

  @BeforeTemplate
  ImmutableMap<K, V> builder(K key, V value) {
    return ImmutableMap.<K, V>builder().put(key, value).build();
  }

  @AfterTemplate
  ImmutableMap<K, V> of(K key, V value) {
    return ImmutableMap.of(key, value);
  }
}
