// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableMap;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Map;

class SingletonMap<K, V> {

  @BeforeTemplate
  Map<K, V> mapOf(K key, V value) {
    return Map.of(key, value);
  }

  /* TODO enable after https://github.com/google/error-prone/issues/1320 is fixed
  @BeforeTemplate
  Map<K, V> singletonMap(
      @Matches(IsNonNullMatcher.class) K key, @Matches(IsNonNullMatcher.class) V value) {
    return Collections.singletonMap(key, value);
  }
  */

  @AfterTemplate
  ImmutableMap<K, V> immutableMapOf(K key, V value) {
    return ImmutableMap.of(key, value);
  }
}
