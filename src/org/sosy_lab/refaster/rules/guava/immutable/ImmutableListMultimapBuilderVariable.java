// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

/** ImmutableListMultimap behaves like ImmutableMultimap but is more clear. */
@SuppressWarnings({"unused", "ModifiedButNotUsed"})
class ImmutableListMultimapBuilderVariable<K, V> {

  @BeforeTemplate
  void immutableMultimap() {
    ImmutableMultimap.Builder<K, V> builder = ImmutableMultimap.builder();
  }

  @BeforeTemplate
  void immutableListMultimapBuilder() {
    ImmutableMultimap.Builder<K, V> builder = ImmutableListMultimap.builder();
  }

  @AfterTemplate
  void immutableListMultimap() {
    ImmutableListMultimap.Builder<K, V> builder = ImmutableListMultimap.builder();
  }
}
