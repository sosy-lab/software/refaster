// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableSet;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collections;
import java.util.Set;

class EmptySet<T> {

  @BeforeTemplate
  Set<T> emptySet() {
    return Collections.emptySet();
  }

  @SuppressWarnings("unchecked")
  @BeforeTemplate
  Set<T> emptySetField() {
    return Collections.EMPTY_SET;
  }

  @BeforeTemplate
  Set<T> setOf() {
    return Set.of();
  }

  @AfterTemplate
  Set<T> immutableSet() {
    return ImmutableSet.of();
  }
}
