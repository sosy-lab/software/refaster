// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.guava.immutable;

import com.google.common.collect.ImmutableMap;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Map;

class ImmutableMapCopyOf<K, V> {

  @BeforeTemplate
  Map<K, V> usingBuilder(Map<K, V> m) {
    ImmutableMap.Builder<K, V> builder = ImmutableMap.builder();
    builder.putAll(m);
    return builder.build();
  }

  @BeforeTemplate
  Map<K, V> mapCopyOf(Map<K, V> m) {
    return Map.copyOf(m);
  }

  @AfterTemplate
  ImmutableMap<K, V> immutableMapCopyOf(Map<K, V> m) {
    return ImmutableMap.copyOf(m);
  }
}
