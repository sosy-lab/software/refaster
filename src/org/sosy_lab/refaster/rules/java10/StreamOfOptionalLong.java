// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java10;

import com.google.common.collect.Streams;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.OptionalLong;
import java.util.stream.LongStream;

class StreamOfOptionalLong {

  @BeforeTemplate
  public LongStream ifEmpty(OptionalLong optional) {
    return optional.isEmpty() ? LongStream.empty() : LongStream.of(optional.orElseThrow());
  }

  @BeforeTemplate
  public LongStream ifPresent(OptionalLong optional) {
    return optional.isPresent() ? LongStream.of(optional.orElseThrow()) : LongStream.empty();
  }

  @BeforeTemplate
  public LongStream guavaStream(OptionalLong optional) {
    return Streams.stream(optional);
  }

  @AfterTemplate
  public LongStream stream(OptionalLong optional) {
    return optional.stream();
  }
}
