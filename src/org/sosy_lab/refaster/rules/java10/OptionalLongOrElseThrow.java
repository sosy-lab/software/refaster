// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java10;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.OptionalLong;

class OptionalLongOrElseThrow {

  @BeforeTemplate
  long get(OptionalLong o) {
    // considered deprecated, cf. Javadoc
    return o.getAsLong();
  }

  @AfterTemplate
  long orElseThrow(OptionalLong o) {
    return o.orElseThrow();
  }
}
