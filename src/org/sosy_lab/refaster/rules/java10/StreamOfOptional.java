// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java10;

import com.google.common.collect.Streams;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Optional;
import java.util.stream.Stream;

class StreamOfOptional<T> {

  @BeforeTemplate
  public Stream<T> ifEmpty(Optional<T> optional) {
    return optional.isEmpty() ? Stream.empty() : Stream.of(optional.orElseThrow());
  }

  @BeforeTemplate
  public Stream<T> ifPresent(Optional<T> optional) {
    return optional.isPresent() ? Stream.of(optional.orElseThrow()) : Stream.empty();
  }

  @BeforeTemplate
  public Stream<T> ofNullable(Optional<T> optional) {
    return Stream.ofNullable(optional.orElse(null));
  }

  @BeforeTemplate
  public Stream<T> guavaStream(Optional<T> optional) {
    return Streams.stream(optional);
  }

  @AfterTemplate
  public Stream<T> stream(Optional<T> optional) {
    return optional.stream();
  }
}
