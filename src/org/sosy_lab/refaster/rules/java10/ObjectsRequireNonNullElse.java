// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java10;

import com.google.common.base.MoreObjects;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Objects;

class ObjectsRequireNonNullElse {

  @BeforeTemplate
  Object firstNonNull(Object a, Object b) {
    return MoreObjects.firstNonNull(a, b);
  }

  @AfterTemplate
  Object requireNonNullElse(Object a, Object b) {
    return Objects.requireNonNullElse(a, b);
  }
}
