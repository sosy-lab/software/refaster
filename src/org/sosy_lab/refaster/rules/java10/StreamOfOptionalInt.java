// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java10;

import com.google.common.collect.Streams;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.OptionalInt;
import java.util.stream.IntStream;

class StreamOfOptionalInt {

  @BeforeTemplate
  public IntStream ifEmpty(OptionalInt optional) {
    return optional.isEmpty() ? IntStream.empty() : IntStream.of(optional.orElseThrow());
  }

  @BeforeTemplate
  public IntStream ifPresent(OptionalInt optional) {
    return optional.isPresent() ? IntStream.of(optional.orElseThrow()) : IntStream.empty();
  }

  @BeforeTemplate
  public IntStream guavaStream(OptionalInt optional) {
    return Streams.stream(optional);
  }

  @AfterTemplate
  public IntStream stream(OptionalInt optional) {
    return optional.stream();
  }
}
