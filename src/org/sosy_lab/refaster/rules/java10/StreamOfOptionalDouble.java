// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java10;

import com.google.common.collect.Streams;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.OptionalDouble;
import java.util.stream.DoubleStream;

class StreamOfOptionalDouble {

  @BeforeTemplate
  public DoubleStream ifEmpty(OptionalDouble optional) {
    return optional.isEmpty() ? DoubleStream.empty() : DoubleStream.of(optional.orElseThrow());
  }

  @BeforeTemplate
  public DoubleStream ifPresent(OptionalDouble optional) {
    return optional.isPresent() ? DoubleStream.of(optional.orElseThrow()) : DoubleStream.empty();
  }

  @BeforeTemplate
  public DoubleStream guavaStream(OptionalDouble optional) {
    return Streams.stream(optional);
  }

  @AfterTemplate
  public DoubleStream stream(OptionalDouble optional) {
    return optional.stream();
  }
}
