// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// The original source of this file is https://errorprone.info/docs/refaster
//
// SPDX-FileCopyrightText: The Error Prone Authors
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class MathMinInt {

  @BeforeTemplate
  int greater(int a, int b) {
    return a > b ? b : a;
  }

  @BeforeTemplate
  int greaterOrEqual(int a, int b) {
    return a >= b ? b : a;
  }

  @BeforeTemplate
  int less(int a, int b) {
    return a < b ? a : b;
  }

  @BeforeTemplate
  int lessOrEqual(int a, int b) {
    return a <= b ? a : b;
  }

  @AfterTemplate
  int min(int a, int b) {
    return Math.min(a, b);
  }
}
