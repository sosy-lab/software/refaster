// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// The original source of this file is https://errorprone.info/docs/refaster
//
// SPDX-FileCopyrightText: The Error Prone Authors
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.MayOptionallyUse;
import com.google.errorprone.refaster.annotation.Placeholder;
import java.util.Set;

abstract class SetIfAddAndMoreElse<E> {

  @Placeholder
  abstract void doAfterAdd(@MayOptionallyUse Set<E> set, @MayOptionallyUse E element);

  @Placeholder
  abstract void doElse(@MayOptionallyUse Set<E> set, @MayOptionallyUse E element);

  @BeforeTemplate
  void ifNotContainsThenAdd(Set<E> set, E element) {
    if (!set.contains(element)) {
      set.add(element);
      doAfterAdd(set, element);
    } else {
      doElse(set, element);
    }
  }

  @AfterTemplate
  void ifAdd(Set<E> set, E element) {
    if (set.add(element)) {
      doAfterAdd(set, element);
    } else {
      doElse(set, element);
    }
  }
}
