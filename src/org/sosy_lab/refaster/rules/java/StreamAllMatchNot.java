// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.Placeholder;
import java.util.stream.Stream;

abstract class StreamAllMatchNot<T> {

  @Placeholder(allowsIdentity = true)
  abstract boolean pred(T e);

  @BeforeTemplate
  boolean allMatchNot(Stream<T> s) {
    return s.allMatch(e -> !pred(e));
  }

  @AfterTemplate
  boolean noneMatch(Stream<T> s) {
    return s.noneMatch(e -> pred(e));
  }
}
