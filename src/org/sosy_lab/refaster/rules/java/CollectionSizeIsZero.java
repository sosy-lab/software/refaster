// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.AlsoNegation;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collection;

class CollectionSizeIsZero<T> {

  @BeforeTemplate
  boolean sizeEqualsZero(Collection<T> c) {
    return c.size() == 0;
  }

  @BeforeTemplate
  boolean sizeLessOrEqualZero(Collection<T> c) {
    // relevant as negated case "c.size() > 0"
    return c.size() <= 0;
  }

  @AfterTemplate
  @AlsoNegation
  boolean isEmpty(Collection<T> c) {
    return c.isEmpty();
  }
}
