// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.MayOptionallyUse;
import com.google.errorprone.refaster.annotation.Placeholder;
import java.util.Iterator;
import java.util.List;

abstract class ForEachLoop<T> {

  @Placeholder(allowsIdentity = true)
  abstract void prefix();

  @Placeholder(allowsIdentity = true)
  abstract void body(@MayOptionallyUse T element);

  @BeforeTemplate
  void whileLoop(Iterable<T> iterable) {
    Iterator<T> it = iterable.iterator();
    while (it.hasNext()) {
      prefix();
      T element = it.next();
      body(element);
    }
  }

  @BeforeTemplate
  void forLoopIterator(Iterable<T> iterable) {
    for (Iterator<T> it = iterable.iterator(); it.hasNext(); ) {
      prefix();
      T element = it.next();
      body(element);
    }
  }

  @BeforeTemplate
  void forLoopIndex(List<T> iterable) {
    for (int i = 0; i < iterable.size(); i++) {
      prefix();
      T element = iterable.get(i);
      body(element);
    }
  }

  @AfterTemplate
  void forEachLoop(Iterable<T> iterable) {
    for (T element : iterable) {
      prefix();
      body(element);
    }
  }
}
