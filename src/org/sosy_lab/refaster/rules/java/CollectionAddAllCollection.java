// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// The original source of this file is https://errorprone.info/docs/refaster
//
// SPDX-FileCopyrightText: The Error Prone Authors
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collection;

class CollectionAddAllCollection<E> {

  @BeforeTemplate
  void addInLoop(Collection<E> source, Collection<E> target) {
    for (E e : source) {
      target.add(e);
    }
  }

  @AfterTemplate
  void addAll(Collection<E> source, Collection<E> target) {
    target.addAll(source);
  }
}
