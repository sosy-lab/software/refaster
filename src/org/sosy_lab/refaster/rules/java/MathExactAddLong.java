// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// The original source of this file is https://errorprone.info/docs/refaster
//
// SPDX-FileCopyrightText: The Error Prone Authors
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.common.math.LongMath;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class MathExactAddLong {

  @BeforeTemplate
  long guava(long a, long b) {
    return LongMath.checkedAdd(a, b);
  }

  @AfterTemplate
  long java(long a, long b) {
    return Math.addExact(a, b);
  }
}
