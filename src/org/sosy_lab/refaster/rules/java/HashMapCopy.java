// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings({"unused", "ModifiedButNotUsed"})
class HashMapCopy<K, V> {

  @BeforeTemplate
  void createAndAddAll(Map<K, V> source) {
    Map<K, V> map = new HashMap<>();
    map.putAll(source);
  }

  @BeforeTemplate
  void createSizedAndAddAll(Map<K, V> source) {
    Map<K, V> map = new HashMap<>(source.size());
    map.putAll(source);
  }

  @AfterTemplate
  void createCopyOf(Map<K, V> source) {
    Map<K, V> map = new HashMap<>(source);
  }
}
