// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.nio.file.Path;
import java.nio.file.Paths;

class PathResolve {

  @BeforeTemplate
  Path resolvePath(Path path, String other) {
    return path.resolve(Paths.get(other));
  }

  @AfterTemplate
  Path resolveString(Path path, String other) {
    return path.resolve(other);
  }
}
