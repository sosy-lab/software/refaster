// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@SuppressWarnings({"unused", "ModifiedButNotUsed"})
class ArrayListCopy<T> {

  @BeforeTemplate
  void createAndAddAll(Collection<T> source) {
    List<T> list = new ArrayList<>();
    list.addAll(source);
  }

  @BeforeTemplate
  void createSizedAndAddAll(Collection<T> source) {
    List<T> list = new ArrayList<>(source.size());
    list.addAll(source);
  }

  @AfterTemplate
  void createCopyOf(Collection<T> source) {
    List<T> list = new ArrayList<>(source);
  }
}
