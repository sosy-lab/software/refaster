// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Map;

class MapContainsValue<V> {

  @BeforeTemplate
  boolean valuesContains(Map<?, V> m, V value) {
    return m.values().contains(value);
  }

  @AfterTemplate
  boolean containsValue(Map<?, V> m, V value) {
    return m.containsValue(value);
  }
}
