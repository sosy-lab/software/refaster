// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.common.collect.Sets;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

class NewTreeSetWithComparator<E> {

  @BeforeTemplate
  Set<E> newTreeSetMethod(Comparator<E> comparator) {
    return Sets.newTreeSet(comparator);
  }

  @AfterTemplate
  Set<E> newTreeSet(Comparator<E> comparator) {
    return new TreeSet<>(comparator);
  }
}
