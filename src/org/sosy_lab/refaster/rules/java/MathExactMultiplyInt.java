// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// The original source of this file is https://errorprone.info/docs/refaster
//
// SPDX-FileCopyrightText: The Error Prone Authors
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.common.math.IntMath;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class MathExactMultiplyInt {

  @BeforeTemplate
  long guava(int a, int b) {
    return IntMath.checkedMultiply(a, b);
  }

  @AfterTemplate
  long java(int a, int b) {
    return Math.multiplyExact(a, b);
  }
}
