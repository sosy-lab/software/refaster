// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.Refaster;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.AlsoNegation;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class RedundantNullCheck<T> {

  @BeforeTemplate
  boolean checkNullAndInstanceOf(Object o) {
    return o != null && Refaster.<T>isInstance(o);
  }

  @AfterTemplate
  @AlsoNegation
  boolean checkInstanceOf(Object o) {
    return Refaster.<T>isInstance(o);
  }
}
