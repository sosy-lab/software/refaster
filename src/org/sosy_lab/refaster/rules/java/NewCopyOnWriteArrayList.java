// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.common.collect.Lists;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

class NewCopyOnWriteArrayList<E> {

  @BeforeTemplate
  List<E> newCopyOnWriteArrayListMethod() {
    return Lists.newCopyOnWriteArrayList();
  }

  @AfterTemplate
  List<E> newCopyOnWriteArrayList() {
    return new CopyOnWriteArrayList<>();
  }
}
