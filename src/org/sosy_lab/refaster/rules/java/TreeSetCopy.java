// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

@SuppressWarnings({"unused", "ModifiedButNotUsed"})
class TreeSetCopy<T extends Comparable<T>> {

  @BeforeTemplate
  void createAndAddAll(Collection<T> source) {
    Set<T> set = new TreeSet<>();
    set.addAll(source);
  }

  @AfterTemplate
  void createCopyOf(Collection<T> source) {
    Set<T> set = new TreeSet<>(source);
  }
}
