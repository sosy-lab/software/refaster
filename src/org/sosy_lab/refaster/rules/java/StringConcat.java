// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class StringConcat {

  @BeforeTemplate
  String concatMethod(String s1, String s2) {
    return s1.concat(s2);
  }

  @BeforeTemplate
  String concatOperatorToString(String s1, Object s2) {
    return s1 + s2.toString();
  }

  @AfterTemplate
  String concatOperator(String s1, Object s2) {
    return s1 + s2;
  }
}
