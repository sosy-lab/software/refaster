// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// The original source of this file is https://errorprone.info/docs/refaster
//
// SPDX-FileCopyrightText: The Error Prone Authors
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.AlsoNegation;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class StringIsEmpty {

  @BeforeTemplate
  boolean equalsEmptyString(String s) {
    return s.equals("");
  }

  /* TODO enable after https://github.com/google/error-prone/issues/1320 is fixed
  @BeforeTemplate
  boolean emptyStringEquals(@Matches(IsNonNullMatcher.class.class) String s) {
    return "".equals(s);
  }
  */

  @BeforeTemplate
  boolean lengthEqualsZero(String s) {
    return s.length() == 0;
  }

  @BeforeTemplate
  boolean lengthLessOrEqualZero(String s) {
    // relevant as negated case "s.length() > 0"
    return s.length() <= 0;
  }

  @AfterTemplate
  @AlsoNegation
  boolean isEmpty(String s) {
    return s.isEmpty();
  }
}
