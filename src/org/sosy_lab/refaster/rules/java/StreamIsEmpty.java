// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.AlsoNegation;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.stream.Stream;

class StreamIsEmpty<T> {

  @BeforeTemplate
  boolean countEqualsZero(Stream<T> s) {
    return s.count() == 0;
  }

  @BeforeTemplate
  boolean sizeLessOrEqualZero(Stream<T> s) {
    // relevant as negated case "s.count() > 0"
    return s.count() <= 0;
  }

  @AfterTemplate
  @AlsoNegation
  boolean isEmpty(Stream<T> s) {
    // limit(1) for short-circuiting
    return s.limit(1).count() == 0;
  }
}
