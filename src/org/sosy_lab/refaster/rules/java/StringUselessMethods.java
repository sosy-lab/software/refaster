// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// The original source of this file is https://errorprone.info/docs/refaster
//
// SPDX-FileCopyrightText: The Error Prone Authors
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.matchers.CompileTimeConstantExpressionMatcher;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.Matches;

class StringUselessMethods {

  @BeforeTemplate
  String internedLiteral(@Matches(CompileTimeConstantExpressionMatcher.class) String s) {
    return s.intern();
  }

  @BeforeTemplate
  @SuppressWarnings("illegalinstantiation")
  String newString(String s) {
    return new String(s);
  }

  @BeforeTemplate
  String toString(String s) {
    return s.toString();
  }

  @AfterTemplate
  String direct(String s) {
    return s;
  }
}
