// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class StringConcatInteger1 {

  @BeforeTemplate
  String concatValueOf(String s, int o) {
    return String.valueOf(o) + s;
  }

  @BeforeTemplate
  String toString(String s, int o) {
    return Integer.toString(o) + s;
  }

  @AfterTemplate
  String concat(String s, int o) {
    return o + s;
  }
}
