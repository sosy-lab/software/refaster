// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.common.collect.Sets;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

class NewCopyOnWriteArraySet<E> {

  @BeforeTemplate
  Set<E> newCopyOnWriteArraySetMethod() {
    return Sets.newCopyOnWriteArraySet();
  }

  @AfterTemplate
  Set<E> newCopyOnWriteArraySet() {
    return new CopyOnWriteArraySet<>();
  }
}
