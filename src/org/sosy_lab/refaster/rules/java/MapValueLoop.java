// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.Placeholder;
import java.util.Map;

abstract class MapValueLoop<K, V> {

  @Placeholder
  abstract void doSomething(V v);

  @BeforeTemplate
  void mapEntryLoopWithVariable(Map<K, V> map) {
    for (Map.Entry<K, V> entry : map.entrySet()) {
      V value = entry.getValue();
      doSomething(value);
    }
  }

  @BeforeTemplate
  void mapEntryLoop(Map<K, V> map) {
    for (Map.Entry<K, V> entry : map.entrySet()) {
      doSomething(entry.getValue());
    }
  }

  @AfterTemplate
  void mapValueLoop(Map<K, V> map) {
    for (V value : map.values()) {
      doSomething(value);
    }
  }
}
