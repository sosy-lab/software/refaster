// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.common.collect.Sets;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

class NewTreeSetFromCollection<E extends Comparable<E>> {

  @BeforeTemplate
  Set<E> newTreeSetMethod(Collection<E> source) {
    return Sets.newTreeSet(source);
  }

  @AfterTemplate
  Set<E> newTreeSet(Collection<E> source) {
    return new TreeSet<>(source);
  }
}
