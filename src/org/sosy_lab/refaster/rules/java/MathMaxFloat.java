// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// The original source of this file is https://errorprone.info/docs/refaster
//
// SPDX-FileCopyrightText: The Error Prone Authors
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class MathMaxFloat {

  @BeforeTemplate
  float greater(float a, float b) {
    return a > b ? a : b;
  }

  @BeforeTemplate
  float greaterOrEqual(float a, float b) {
    return a >= b ? a : b;
  }

  @BeforeTemplate
  float less(float a, float b) {
    return a < b ? b : a;
  }

  @BeforeTemplate
  float lessOrEqual(float a, float b) {
    return a <= b ? b : a;
  }

  @AfterTemplate
  float max(float a, float b) {
    return Math.max(a, b);
  }
}
