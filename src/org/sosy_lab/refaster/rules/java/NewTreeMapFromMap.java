// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.common.collect.Maps;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

class NewTreeMapFromMap<K, V> {

  @BeforeTemplate
  Map<K, V> newTreeMapMethod(SortedMap<K, V> source) {
    return Maps.newTreeMap(source);
  }

  @AfterTemplate
  Map<K, V> newTreeMap(SortedMap<K, V> source) {
    return new TreeMap<>(source);
  }
}
