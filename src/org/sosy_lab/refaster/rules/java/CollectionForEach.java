// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.Collection;
import java.util.function.Consumer;

class CollectionForEach<T> {

  @BeforeTemplate
  void streamForEach(Collection<T> coll, Consumer<? super T> f) {
    coll.stream().forEach(f);
  }

  @AfterTemplate
  void forEach(Collection<T> coll, Consumer<? super T> f) {
    coll.forEach(f);
  }
}
