// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// The original source of this file is https://errorprone.info/docs/refaster
//
// SPDX-FileCopyrightText: The Error Prone Authors
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class MathMinLong {

  @BeforeTemplate
  long greater(long a, long b) {
    return a > b ? b : a;
  }

  @BeforeTemplate
  long greaterOrEqual(long a, long b) {
    return a >= b ? b : a;
  }

  @BeforeTemplate
  long less(long a, long b) {
    return a < b ? a : b;
  }

  @BeforeTemplate
  long lessOrEqual(long a, long b) {
    return a <= b ? a : b;
  }

  @AfterTemplate
  long min(long a, long b) {
    return Math.min(a, b);
  }
}
