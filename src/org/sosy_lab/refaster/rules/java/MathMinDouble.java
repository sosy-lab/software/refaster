// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// The original source of this file is https://errorprone.info/docs/refaster
//
// SPDX-FileCopyrightText: The Error Prone Authors
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class MathMinDouble {

  @BeforeTemplate
  double greater(double a, double b) {
    return a > b ? b : a;
  }

  @BeforeTemplate
  double greaterOrEqual(double a, double b) {
    return a >= b ? b : a;
  }

  @BeforeTemplate
  double less(double a, double b) {
    return a < b ? a : b;
  }

  @BeforeTemplate
  double lessOrEqual(double a, double b) {
    return a <= b ? a : b;
  }

  @AfterTemplate
  double min(double a, double b) {
    return Math.min(a, b);
  }
}
