// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;

class StringConcatBoolean2 {

  @BeforeTemplate
  String concatValueOf(String s, boolean o) {
    return s + String.valueOf(o);
  }

  @BeforeTemplate
  String toString(String s, boolean o) {
    return s + Boolean.toString(o);
  }

  @AfterTemplate
  String concat(String s, boolean o) {
    return s + o;
  }
}
