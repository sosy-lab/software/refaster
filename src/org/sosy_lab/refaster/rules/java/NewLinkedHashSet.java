// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.java;

import com.google.common.collect.Sets;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import java.util.LinkedHashSet;
import java.util.Set;

class NewLinkedHashSet<E> {

  @BeforeTemplate
  Set<E> newLinkedHashSetMethod() {
    return Sets.newLinkedHashSet();
  }

  @AfterTemplate
  Set<E> newLinkedHashSet() {
    return new LinkedHashSet<>();
  }
}
