// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.truth;

import static com.google.common.truth.Truth.assertThat;

import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.UseImportPolicy;
import org.junit.Assert;

class ByteIsEqual {

  @BeforeTemplate
  void assertTrueIsEqual(byte a, byte b) {
    Assert.assertTrue(a == b);
  }

  @BeforeTemplate
  void assertFalseIsNotEqual(byte a, byte b) {
    Assert.assertFalse(a != b);
  }

  @BeforeTemplate
  void assertEquals(byte a, byte b) {
    Assert.assertEquals(b, a);
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  void isSameInstanceAs(byte a, byte b) {
    assertThat(a).isEqualTo(b);
  }
}
