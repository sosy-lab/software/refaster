// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.truth;

import static com.google.common.truth.Truth.assertThat;
import static com.google.common.truth.Truth.assert_;

import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.UseImportPolicy;
import org.junit.Assert;

class Fail {

  @BeforeTemplate
  void fail() {
    Assert.fail();
  }

  @BeforeTemplate
  void assertTrueFalse() {
    Assert.assertTrue(false);
  }

  @BeforeTemplate
  void assertFalseTrue() {
    Assert.assertFalse(true);
  }

  @BeforeTemplate
  void assertFalseIsTrue() {
    assertThat(false).isTrue();
  }

  @BeforeTemplate
  void assertTrueIsFalse() {
    assertThat(true).isFalse();
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  void assertFail() {
    assert_().fail();
  }
}
