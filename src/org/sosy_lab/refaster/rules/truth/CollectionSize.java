// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.truth;

import static com.google.common.truth.Truth.assertThat;

import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.UseImportPolicy;
import java.util.Collection;
import org.junit.Assert;

class CollectionSize<T> {

  @BeforeTemplate
  void assertTrueSizeEqual(Collection<T> c, int size) {
    Assert.assertTrue(c.size() == size);
  }

  @BeforeTemplate
  void assertEqualsSize(Collection<T> c, int size) {
    Assert.assertEquals(size, c.size());
  }

  @BeforeTemplate
  void assertSizeEquals(Collection<T> c, int size) {
    Assert.assertEquals(c.size(), size);
  }

  @BeforeTemplate
  void assertSizeIsEqualTo(Collection<T> c, int size) {
    assertThat(c.size()).isEqualTo(size);
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  void assertThatIsEmpty(Collection<T> c, int size) {
    assertThat(c).hasSize(size);
  }
}
