// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.truth;

import static com.google.common.truth.Truth.assertThat;

import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.UseImportPolicy;
import org.junit.Assert;

class ObjectIsNull {

  @BeforeTemplate
  void assertTrueIsNull(Object o) {
    Assert.assertTrue(o == null);
  }

  @BeforeTemplate
  void assertFalseIsNotNull(Object o) {
    Assert.assertFalse(o != null);
  }

  @BeforeTemplate
  void assertNull(Object o) {
    Assert.assertNull(o);
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  void isNull(Object o) {
    assertThat(o).isNull();
  }
}
