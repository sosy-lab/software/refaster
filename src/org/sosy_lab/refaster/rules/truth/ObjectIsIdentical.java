// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.truth;

import static com.google.common.truth.Truth.assertThat;

import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.UseImportPolicy;
import org.junit.Assert;

class ObjectIsIdentical {

  @BeforeTemplate
  void assertTrueIsIdentical(Object o1, Object o2) {
    Assert.assertTrue(o1 == o2);
  }

  @BeforeTemplate
  void assertFalseIsNotIdentical(Object o1, Object o2) {
    Assert.assertFalse(o1 != o2);
  }

  @BeforeTemplate
  void assertSame(Object o1, Object o2) {
    Assert.assertSame(o2, o1);
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  void isSameInstanceAs(Object o1, Object o2) {
    assertThat(o1).isSameInstanceAs(o2);
  }
}
