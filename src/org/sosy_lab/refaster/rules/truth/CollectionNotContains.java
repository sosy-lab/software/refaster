// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.truth;

import static com.google.common.truth.Truth.assertThat;

import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.UseImportPolicy;
import java.util.Collection;
import org.junit.Assert;

class CollectionNotContains<T> {

  @BeforeTemplate
  void assertFalseContains(Collection<T> c, Object element) {
    Assert.assertFalse(c.contains(element));
  }

  @BeforeTemplate
  void assertTrueNotContains(Collection<T> c, Object element) {
    Assert.assertTrue(!c.contains(element));
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  void assertThatDoesNotContain(Collection<T> c, Object element) {
    assertThat(c).doesNotContain(element);
  }
}
