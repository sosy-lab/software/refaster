// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.truth;

import static com.google.common.truth.Truth.assertThat;

import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.UseImportPolicy;
import org.junit.Assert;

class ByteIsNotEqual {

  @BeforeTemplate
  void assertFalseIsEqual(byte a, byte b) {
    Assert.assertFalse(a == b);
  }

  @BeforeTemplate
  void assertTrueIsNotEqual(byte a, byte b) {
    Assert.assertTrue(a != b);
  }

  @BeforeTemplate
  void assertNotEquals(byte a, byte b) {
    Assert.assertNotEquals(b, a);
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  void isEqualTo(byte a, byte b) {
    assertThat(a).isNotEqualTo(b);
  }
}
