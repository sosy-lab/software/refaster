// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.truth;

import static com.google.common.truth.Truth.assertWithMessage;

import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.UseImportPolicy;
import org.junit.Assert;

class ObjectIsEqualWithMessage {

  @BeforeTemplate
  void assertEquals(String msg, Object o1, Object o2) {
    Assert.assertEquals(msg, o1, o2);
  }

  @BeforeTemplate
  void assertArrayEquals(String msg, Object[] o1, Object[] o2) {
    Assert.assertArrayEquals(msg, o2, o1); // Truth does meaningful array comparison by default
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  void isEqualTo(String msg, Object o1, Object o2) {
    assertWithMessage(msg).that(o1).isEqualTo(o2);
  }
}
