// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.truth;

import static com.google.common.truth.Truth.assertThat;

import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.UseImportPolicy;
import org.junit.Assert;

class DoubleIsNotEqual {

  @BeforeTemplate
  void assertFalseIsEqual(double a, double b) {
    Assert.assertFalse(a == b);
  }

  @BeforeTemplate
  void assertTrueIsNotEqual(double a, double b) {
    Assert.assertTrue(a != b);
  }

  @BeforeTemplate
  void assertNotEquals(double a, double b) {
    Assert.assertNotEquals(b, a);
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  void isEqualTo(double a, double b) {
    assertThat(a).isNotEqualTo(b);
  }
}
