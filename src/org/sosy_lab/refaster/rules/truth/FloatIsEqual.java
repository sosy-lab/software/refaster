// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.truth;

import static com.google.common.truth.Truth.assertThat;

import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.UseImportPolicy;
import org.junit.Assert;

class FloatIsEqual {

  @BeforeTemplate
  void assertTrueIsEqual(float a, float b) {
    Assert.assertTrue(a == b);
  }

  @BeforeTemplate
  void assertFalseIsNotEqual(float a, float b) {
    Assert.assertFalse(a != b);
  }

  @SuppressWarnings({"deprecation", "DoNotCall"})
  @BeforeTemplate
  void assertEquals(float a, float b) {
    Assert.assertEquals(b, a);
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  void isSameInstanceAs(float a, float b) {
    assertThat(a).isEqualTo(b);
  }
}
