// This file is part of SoSy-Lab Refaster Rules,
// a collection of useful rules for Google Refaster:
// https://gitlab.com/sosy-lab/software/refaster
//
// SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.refaster.rules.truth;

import static com.google.common.truth.Truth.assertWithMessage;

import com.google.errorprone.refaster.ImportPolicy;
import com.google.errorprone.refaster.annotation.AfterTemplate;
import com.google.errorprone.refaster.annotation.BeforeTemplate;
import com.google.errorprone.refaster.annotation.UseImportPolicy;
import org.junit.Assert;

class FailWithMessage {

  @BeforeTemplate
  void fail(String msg) {
    Assert.fail(msg);
  }

  @BeforeTemplate
  void assertTrueFalse(String msg) {
    Assert.assertTrue(msg, false);
  }

  @BeforeTemplate
  void assertFalseTrue(String msg) {
    Assert.assertFalse(msg, true);
  }

  @BeforeTemplate
  void assertFalseIsTrue(String msg) {
    assertWithMessage(msg).that(false).isTrue();
  }

  @BeforeTemplate
  void assertTrueIsFalse(String msg) {
    assertWithMessage(msg).that(true).isFalse();
  }

  @AfterTemplate
  @UseImportPolicy(ImportPolicy.STATIC_IMPORT_ALWAYS)
  void assertFail(String msg) {
    assertWithMessage(msg).fail();
  }
}
