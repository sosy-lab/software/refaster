<!--
This file is part of SoSy-Lab Refaster Rules,
a collection of useful rules for Google Refaster:
https://gitlab.com/sosy-lab/software/refaster

SPDX-FileCopyrightText: 2019-2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# SoSy-Lab Refaster Rules

[![Build Status](https://gitlab.com/sosy-lab/software/refaster/badges/master/pipeline.svg "Build Status")](https://gitlab.com/sosy-lab/software/refaster/pipelines)
[![Apache 2.0 License](https://img.shields.io/badge/license-Apache--2-brightgreen.svg?style=flat)](http://www.apache.org/licenses/LICENSE-2.0)

This project contains several useful refactorings for Java code
expressed as rules for [Google Refaster](https://errorprone.info/docs/refaster),
which is part of [Google Error Prone](https://errorprone.info/).

To automatically apply these refactoring rules to a codebase,
they first need to be compiled into a special rule file with Refaster,
and then can be passed as parameter when compiling the codebase
with Error Prone.

Compiling the rules can be done with one of the following command lines:

    ant build-refaster-rule -Drefaster.source.file=<SINGLE_JAVA_FILE_WITH_RULE>
    ant build-refaster-rule -Drefaster.source.pattern=<LIST_OF_FILE_PATTERNS>

In both cases, paths are relative to `src/`. Examples:

    ant build-refaster-rule -Drefaster.source.file=org/sosy_lab/refaster/rules/java/AssertionFalse.java
    ant build-refaster-rule -Drefaster.source.pattern="**/java/*.java **/guava/**/*.java"

The output file is named `rule.refaster` by default, it can be overridden with `-Drefaster.rule.file=<FILE>`.

The resulting rule file is a binary file
that can be used only with a matching version of Google Error Prone.
To force a specific Error Prone version for rule compilation,
use `-Derrorprone.version=<VERSION>` (lowest supported version is 2.3.3).
